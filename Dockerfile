# hadolint ignore=DL3007
FROM debian:bookworm-slim

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker
ENV HOME /home/user
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV RYE_NO_AUTO_INSTALL=1
ENV PATH="$HOME/.rye/shims:$PATH"

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="rye" \
    org.opencontainers.image.description="A python docker image with rye" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/rye" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/rye" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

# hadolint ignore=DL3008,DL4001,DL4006
RUN apt-get update && apt-get install -y --no-install-recommends \
       ca-certificates \
       curl \
       git \
       wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p ${HOME} \
    && curl -sSf https://rye.astral.sh/get | RYE_VERSION="${VERSION}" RYE_INSTALL_OPTION="--yes" bash \
    && sh -c "$(wget -qO - https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin

WORKDIR /workdir

CMD ["rye", "--version"]
